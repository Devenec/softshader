# SoftShader Executable Format Specification

Version 0.0.1

SoftShader Executable format is a file format for executable shader code. A SoftShader Executable consists of
multiple sections that specify metadata and the executable code.

A byte is 8 bits wide. All values larger than one byte are stored in little-endian order.


## Data Types

The format uses the following data types:

| Name    | Size (bytes) | Description           |
|---------|--------------|-----------------------|
| Float   | 4            | Floating-point        |
| Uint32  | 4            | Unsigned integer      |
| Vector2 | 8            | Floating-point vector |
| Vector3 | 12           | Floating-point vector |
| Vector4 | 16           | Floating-point vector |
| *       | Variable     | Arbitrary data        |


## Structure

A SoftShader Executable consists of multiple sections, each located immediately after the previous one, in the
following order:

- Signature
- Header
- Constants
- Code

### Signature

The first four bytes of a SoftShader Executable form the signature, and contain the following value:

```
0x53 0x66 0x53 0x68
```

### Header

Contains information about the file.

| Name                | Type   | Description                                                                 |
|---------------------|--------|-----------------------------------------------------------------------------|
| File format version | Uint32 | Bits 31..22 specify the major number, bits 21..12 specify the minor number, and bits 11..0 specify the build number. |

### Constants

Contains constant data that is accessed by the executable code.

| Name      | Type   | Description                                     |
|-----------|--------|-------------------------------------------------|
| Data size | Uint32 | The size of the constant data in bytes          |
| Data      | *      | The constant data, absent if *<Data size>* is 0 |

### Code

Contains the executable code.

| Name          | Type   | Description                                                                       |
|---------------|--------|-----------------------------------------------------------------------------------|
| Start address | Uint32 | The address of the first instruction to execute, relative to the beginning of the code |
| Code size     | Uint32 | The size of the code in bytes, must be greater than 0                             |
| Code          | *      | The executable code                                                               |


## Memory Model

A SoftShader Executable operates on a preallocated contiguous memory called *work memory*. Memory addresses
are 32-bit unsigned integers, and bits 30..0 are used for specifying an address. Address `0x00` points to the
beginning of the memory. If bit 31 of an address is set, the address is relative to the address specified by
*Base Pointer* register (see [Address Registers](#address-registers)).

The executable code resides in a separate read-only memory. Instruction addresses are relative to the
beginning of the code.

### External Memory

In addition to work memory, a program can access three external memory areas:

| Name    | Value  | Description                                                             |
|---------|--------|-------------------------------------------------------------------------|
| Input   | `0b00` | Stores input data for the specific invocation of the program, read-only |
| Output  | `0b01` | Stores output data for specific invocation of the program, read-write   |
| Uniform | `0b10` | Stores data common for all invocations of the program, read-only        |

The values in external memory are accessed using *location* indices, starting from location 0x00.


## Registers

### Address Registers

| Name                | Semantic | Value        | Type   | Description                                       |
|---------------------|----------|--------------|--------|---------------------------------------------------|
| Base Pointer        | BP       | `0b00000000` | Uint32 | The value of the register is added to relative memory addresses |
| Instruction Pointer | IP       | `0b00000001` | Uint32 | Points to the instruction at the executable code, to be executed next |
| Return Pointer      | RP       | `0b00000010` | Uint32 | Contains an instruction address used by RET instruction  |


### Floating-Point Registers

| Semantic | Value        | Type    |
|----------|--------------|---------|
| FP0      | `0b00000011` | Vector4 |
| FP1      | `0b00000100` | Vector4 |


## Instructions

### ADD

Add floating-point values

`0`. Adds the floating-point value in register *registerA* to the floating-point value in register
*registerB*, and stores the result to register *registerA*.

| Opcode       | Variant  | count  | registerA | registerB |
|--------------|----------|--------|-----------|-----------|
| `0b00000000` | `0b0000` | 4 bits | 8 bits    | 8 bits    |

`1`. Adds the floating-point value in register *register* to the floating-point value at memory address
*address*, and stores the result to register *register*.

| Opcode       | Variant  | count  | register  | address |
|--------------|----------|--------|-----------|---------|
| `0b00000000` | `0b0001` | 4 bits | 8 bits    | 32 bits |

*count* specifies the type of the value:

| count    | Value Type |
|----------|------------|
| `0b0000` | Float      |
| `0b0001` | Vector2    |
| `0b0010` | Vector3    |
| `0b0011` | Vector4    |

Each register must be one of the [floating-point registers](#floating-point-registers).

### BPA

Add to Base Pointer

Adds the *Uint32* value *value* to *BP*.

| Opcode       | value   |
|--------------|---------|
| `0b00000001` | 32 bits |

### BPS

Subtract from Base Pointer

Subtracts the *Uint32* value *value* from *BP*.

| Opcode       | value   |
|--------------|---------|
| `0b00000010` | 32 bits |

### CALL

Call procedure

Stores the address of the next instruction to *RP*, and stores *instructionAddress* to *IP*. I.e. jumps to the
instruction at address *instructionAddress*.

| Opcode       | instructionAddress |
|--------------|--------------------|
| `0b00000011` | 32 bits            |

### LD

Load a floating-point value from external memory

`0`. Copies a floating-point value from the external memory at *location* to register *register*.

| Opcode       | Variant      | location | register |
|--------------|--------------|----------|----------|
| `0b00000100` | `0b00000000` | 16 bits  | 8 bits   |

`1`. Copies a floating-point value from the external memory at *location* to the memory at address *address*.

| Opcode       | Variant      | location | address |
|--------------|--------------|----------|---------|
| `0b00000100` | `0b00000001` | 16 bits  | 32 bits |

*location* specifies a location in the external memory (see [External Memory](#external-memory)):

| memoryArea | locationIndex |
|------------|---------------|
| 2 bits     | 14 bits       |

Each register must be one of the [floating-point registers](#floating-point-registers).

### MOV

Copies values

`0`. Copies a 32-bit value from the memory at address *address* to register *register*.

| Opcode       | Variant  | *Unused* | register | address |
|--------------|----------|----------|----------|---------|
| `0b00000101` | `0b0000` | 4 bits   | 8 bits   | 32 bits |

`1`. Copies a 32-bit value from register *register* to the memory at address *address*.

| Opcode       | Variant  | *Unused* | address  | register |
|--------------|----------|----------|----------|----------|
| `0b00000101` | `0b0001` | 4 bits   | 32 bits  | 8 bits   |

`2`. Copies *count + 1* 32-bit values from the memory at address *srcAddress* to the memory at address
*dstAddress*.

| Opcode       | Variant  | count    | dstAddress | srcAddress |
|--------------|----------|----------|------------|------------|
| `0b00000101` | `0b0010` | 4 bits   | 32 bits    | 32 bits    |

Each register must be either *BP* or *RP*.

### MOVF

Copies floating-point values

`0`. Copies a floating-point value from the memory at address *address* to register *register*.

| Opcode       | Variant  | count  | register | address |
|--------------|----------|--------|----------|---------|
| `0b00000110` | `0b0000` | 4 bits | 8 bits   | 32 bits |

`1`. Copies a floating-point value from register *register* to the memory at address *address*.

| Opcode       | Variant  | count  | address  | register |
|--------------|----------|--------|----------|----------|
| `0b00000110` | `0b0001` | 4 bits | 32 bits  | 8 bits   |

*count* specifies the type of the value:

| count    | Value Type |
|----------|------------|
| `0b0000` | Float      |
| `0b0001` | Vector2    |
| `0b0010` | Vector3    |
| `0b0011` | Vector4    |

Each register must be one of the [floating-point registers](#floating-point-registers).

### RET

Return procedure

Copies the value of *RP* to *IP*, i.e. jumps to the instruction pointed by the value of *RP*. Jumping to
address `0xFFFFFFFF` exits the program.

| Opcode       |
|--------------|
| `0b00000111` |

### ST

Store a floating-point value to output memory

`0`. Copies a floating-point value from register *register* to the output memory at location *locationIndex*.

| Opcode       | Variant      | locationIndex | register |
|--------------|--------------|---------------|----------|
| `0b00001000` | `0b00000000` | 8 bits        | 8 bits   |

`1`. Copies a floating-point value from the memory at address *address* to the output memory at location
*locationIndex*.

| Opcode       | Variant      | locationIndex | address |
|--------------|--------------|---------------|---------|
| `0b00001000` | `0b00000001` | 8 bits        | 32 bits |

See [External Memory](#external-memory) for more info. Each register must be one of the
[floating-point registers](#floating-point-registers).


## Executable Loading

When loading an executable, before the program execution, the constant data is copied to the work memory at
address `0x00`. Writing to the memory range in which the constant data resides is undefined behaviour.
The [address registers](#address-registers) are set to the following values:

| Name                | Value                                                  |
|---------------------|--------------------------------------------------------|
| Base Pointer        | The memory address immediately after the constant data |
| Instruction Pointer | *<Start address>* (see [Code](#code))                  |
| Return Pointer      | `0xFFFFFFFF`                                           |

The value of every other register is unspecified.
