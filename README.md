# SoftShader

A shader code generator and software shader processor  
Version 0.0.1


## About

SoftShader is written in C++17. The project consists of a shader code generator, a shader disassembler, and a
shader processor.


## Features

### Shader Code Generator

The shader code generator uses [glslang] to generate a parse tree from GLSL source code, from which the
executable shader code is generated. SoftShader uses custom [executable format] code.

### Shader Disassembler

The shader disassembler translates SoftShader [executable format] code to an assembly language code.

### Shader Processor

**Work in progress**


## Dependencies

All dependencies are included in the project.

- [glslang] 8.13.3559
- [{fmt}][fmt] 6.2.0


## Building

### Windows

Tested in Visual Studio 2019

Open *build/soft-shader.sln* in Visual Studio. Currently, external projects
(see [dependencies](#dependencies)) are excluded from solution build, and must be built separately before
building *soft-shader* project.


## Usage

**Work in progress**


## Copyright & License

Copyright 2020 Eetu 'Devenec' Oinasmaa
Licensed under MIT License

The third party source code may be licensed under different licenses, as described in [LICENSE-3RD-PARTY.txt].


[executable format]: soft-shader-executable-format-specification.md
[fmt]: https://fmt.dev/latest/index.html
[glslang]: https://github.com/KhronosGroup/glslang
[LICENSE-3RD-PARTY.txt]: LICENSE-3RD-PARTY.txt
