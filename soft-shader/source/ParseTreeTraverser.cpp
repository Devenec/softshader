/**
 * @file soft-shader/ParseTreeTraverser.cpp
 *
 * SoftShader
 * Copyright 2020 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

#include <soft-shader/ParseTreeTraverser.h>

#include <cstddef>
#include <limits>
#include <stdexcept>

#include <glslang/Include/InfoSink.h>

using namespace SoftShader;

// Interal

static constexpr uint32_t unitSize = sizeof(uint32_t);

static uint32_t calculateTypeSize(const glslang::TType& type);
static void invokeError(const glslang::TSourceLoc& location, const char* context, const char* message);
static uint32_t relativeAddress(const uint32_t address);
static void validateType(const glslang::TIntermTyped* typedNode, const bool allowGlobal = false);


// Private

// Add instruction methods

void ParseTreeTraverser::addInstructionMov(const InstructionOperand& operand0,
	const InstructionOperand& operand1)
{
	assert
	(
		(operand0.isRegister() && operand1.isAddress()) ||
		(operand0.isAddress() && operand1.isRegister()) ||
		(operand0.isAddress() && operand1.isAddress())
	);

	uint32_t variant = 0;

	if(operand0.isAddress())
		variant = operand1.isRegister() ? 1 : 2;

	addInstruction(Opcode::MOV, variant, operand0, operand1);
}

// Processing methods

void ParseTreeTraverser::processBranchReturn(glslang::TIntermBranch* node)
{
	glslang::TIntermTyped* expression = node->getExpression();

	if(expression != nullptr)
	{
		incrementDepth(node);
		expression->traverse(this);
		decrementDepth();
		processLoadOrMove(relativeAddressMask, _evaluatedOperand);
	}

	addInstruction(Opcode::RET);
}

void ParseTreeTraverser::processConstruct(glslang::TIntermAggregate* node)
{
	const uint32_t memoryPointerAtCall = _memoryPointer;

	for(TIntermNode* parameterNode : node->getSequence())
	{
		parameterNode->traverse(this);
		addInstructionMov(relativeAddress(_memoryPointer), _evaluatedOperand);
		_memoryPointer += _evaluatedOperand.count() * unitSize;
	}

	_memoryPointer = memoryPointerAtCall;
	const uint32_t count = static_cast<uint32_t>(node->getVectorSize());
	_evaluatedOperand = InstructionOperand(relativeAddress(_memoryPointer), count);
}

void ParseTreeTraverser::processFunction(glslang::TIntermAggregate* node)
{
	validateType(node, true);
	_activeLabel = node->getName();
	_memoryPointer = calculateTypeSize(node->getType());
	glslang::TIntermSequence& sequence = node->getSequence();
	glslang::TIntermAggregate* parametersNode = sequence[0]->getAsAggregate();

	for(TIntermNode* parameterNode : parametersNode->getSequence())
		parameterNode->traverse(this);

	if(sequence.size() > 1)
	{
		incrementDepth(node);
		sequence[1]->traverse(this);
		decrementDepth();
	}

	if(_instructions.empty() || _instructions.back().opcode != Opcode::RET)
		addInstruction(Opcode::RET);
}

void ParseTreeTraverser::processFunctionCall(glslang::TIntermAggregate* node)
{
	validateType(node, true);
	const uint32_t memoryPointerAtCall = _memoryPointer;
	addInstructionMov(relativeAddress(_memoryPointer), Register::RP);
	_memoryPointer += sizeof(uint32_t);
	const uint32_t returnValueAddress = _memoryPointer;
	_memoryPointer += calculateTypeSize(node->getType());
	incrementDepth(node);

	for(TIntermNode* argumentNode : node->getSequence())
	{
		argumentNode->traverse(this);
		processLoadOrMove(relativeAddress(_memoryPointer), _evaluatedOperand);
		_memoryPointer += calculateTypeSize(argumentNode->getAsTyped()->getType());
	}

	decrementDepth();
	addInstruction(Opcode::BPA, 0, returnValueAddress);
	addInstruction(Opcode::CALL, 0, InstructionOperand(), InstructionOperand(), node->getName());
	addInstruction(Opcode::BPS, 0, returnValueAddress);
	_memoryPointer = memoryPointerAtCall;
	addInstructionMov(Register::RP, relativeAddress(_memoryPointer));

	if(node->getBasicType() == glslang::EbtVoid)
	{
		_evaluatedOperand = invalidAddress;
	}
	else
	{
		const uint32_t count = static_cast<uint32_t>(node->getVectorSize());
		_evaluatedOperand = InstructionOperand(relativeAddress(returnValueAddress), count);
	}
}

void ParseTreeTraverser::processLoadOrMove(const InstructionOperand& destinationOperand,
	const InstructionOperand& sourceOperand)
{
	if(sourceOperand.isLocation())
		addInstructionLd(sourceOperand, destinationOperand);
	else
		processMove(destinationOperand, sourceOperand);
}

void ParseTreeTraverser::processMove(const InstructionOperand& destinationOperand,
	const InstructionOperand& sourceOperand)
{
	if(sourceOperand.isRegister())
		addInstructionMovf(destinationOperand, sourceOperand);
	else
		addInstructionMov(destinationOperand, sourceOperand);
}

void ParseTreeTraverser::processOperationAdd(glslang::TIntermBinary* node)
{
	incrementDepth(node);
	node->getLeft()->traverse(this);

	if(_evaluatedOperand.isAddress())
		addInstructionMovf(Register::FP0, _evaluatedOperand);
	else if(_evaluatedOperand.isLocation())
		addInstructionLd(_evaluatedOperand, Register::FP0);

	node->getRight()->traverse(this);
	decrementDepth();

	if(_evaluatedOperand.isLocation())
	{
		addInstructionLd(_evaluatedOperand, Register::FP1);
		_evaluatedOperand = InstructionOperand(Register::FP1, _evaluatedOperand.count());
	}

	addInstructionAdd(Register::FP0, _evaluatedOperand);
	_evaluatedOperand = InstructionOperand(Register::FP0, _evaluatedOperand.count());
}

void ParseTreeTraverser::processOperationAssign(glslang::TIntermBinary* node)
{
	incrementDepth(node);
	node->getLeft()->traverse(this);
	const InstructionOperand destinationOperand = _evaluatedOperand;
	node->getRight()->traverse(this);
	decrementDepth();

	if(destinationOperand == _evaluatedOperand)
		return;

	if(_evaluatedOperand.isLocation())
	{
		if(destinationOperand.isLocation())
		{
			addInstructionLd(_evaluatedOperand, Register::FP0);
			_evaluatedOperand = InstructionOperand(Register::FP0, _evaluatedOperand.count());
		}
		else
		{
			addInstructionLd(_evaluatedOperand, destinationOperand);
			return;
		}
	}

	if(destinationOperand.isLocation())
		addInstructionSt(destinationOperand, _evaluatedOperand);
	else
		processMove(destinationOperand, _evaluatedOperand);
}

void ParseTreeTraverser::processOperationIndexStructure(const glslang::TIntermBinary* node)
{
	const glslang::TType& type = node->getType();

	if(!type.isBuiltIn())
		invokeError(node->getLoc(), "binary", "unsupported statement");

	uint32_t location = 0;

	switch(type.getQualifier().builtIn)
	{
		case glslang::EbvPosition:
			location = static_cast<uint32_t>(ExternalMemory::Output) << 14;
			break;

		default:
			invokeError(node->getLoc(), type.getBuiltInVariableString(), "unsupported built-in");
			break;
	}

	_evaluatedOperand = InstructionOperand(location, static_cast<uint32_t>(node->getVectorSize()), 0);
}

void ParseTreeTraverser::processSymbol(const glslang::TIntermSymbol* node)
{
	SymbolMap::iterator iterator = _symbols.find(node->getId());

	if(iterator == _symbols.end())
	{
		const uint32_t address = relativeAddress(_memoryPointer);
		_memoryPointer += calculateTypeSize(node->getType());
		auto result = _symbols.emplace(node->getId(), address);
		iterator = result.first;
	}

	const uint32_t count = static_cast<uint32_t>(node->getVectorSize());
	_evaluatedOperand = InstructionOperand(iterator->second.address, count);
}

// Visit methods

bool ParseTreeTraverser::visitAggregate(glslang::TVisit visit, glslang::TIntermAggregate* node)
{
	static_cast<void>(visit);

	switch(node->getOp())
	{
		case glslang::EOpConstructFloat:
		case glslang::EOpConstructVec2:
		case glslang::EOpConstructVec3:
		case glslang::EOpConstructVec4:
			processConstruct(node);
			return false;

		case glslang::EOpFunction:
			processFunction(node);
			return false;

		case glslang::EOpFunctionCall:
			processFunctionCall(node);
			return false;

		case glslang::EOpLinkerObjects:
			return false;

		case glslang::EOpSequence:
			break;

		default:
			invokeError(node->getLoc(), "aggregate", "unsupported statement");
			break;
	}

	return true;
}

bool ParseTreeTraverser::visitBinary(glslang::TVisit visit, glslang::TIntermBinary* node)
{
	static_cast<void>(visit);

	switch(node->getOp())
	{
		case glslang::EOpAdd:
			processOperationAdd(node);
			return false;

		case glslang::EOpAssign:
			processOperationAssign(node);
			return false;

		case glslang::EOpIndexDirectStruct:
			processOperationIndexStructure(node);
			return false;

		default:
			invokeError(node->getLoc(), "binary", "unsupported statement");
			break;
	}

	return true;
}

bool ParseTreeTraverser::visitBranch(glslang::TVisit visit, glslang::TIntermBranch* node)
{
	static_cast<void>(visit);

	switch(node->getFlowOp())
	{
		case glslang::EOpReturn:
			processBranchReturn(node);
			return false;

		default:
			invokeError(node->getLoc(), "jump", "unsupported statement");
			break;
	}

	return true;
}

void ParseTreeTraverser::visitConstantUnion(glslang::TIntermConstantUnion* node)
{
	validateType(node);
	const glslang::TConstUnionArray& constantArray = node->getConstArray();
	const uint32_t offset = static_cast<uint32_t>(_constants.size());
	_constants.reserve(calculateTypeSize(node->getType()));

	for(int i = 0; i < constantArray.size(); ++i)
	{
		const float value = static_cast<float>(constantArray[i].getDConst());
		const std::byte* valueBytes = reinterpret_cast<const std::byte*>(&value);
		_constants.insert(_constants.end(), valueBytes, valueBytes + sizeof(float));
	}

	_evaluatedOperand = InstructionOperand(offset, static_cast<uint32_t>(node->getVectorSize()));
}

bool ParseTreeTraverser::visitLoop(glslang::TVisit visit, glslang::TIntermLoop* node)
{
	static_cast<void>(visit);
	static_cast<void>(node);

	invokeError(node->getLoc(), "iteration", "unsupported statement");
	return false;
}

bool ParseTreeTraverser::visitSelection(glslang::TVisit visit, glslang::TIntermSelection* node)
{
	static_cast<void>(visit);
	static_cast<void>(node);

	invokeError(node->getLoc(), "selection", "unsupported statement");
	return false;
}

bool ParseTreeTraverser::visitSwitch(glslang::TVisit visit, glslang::TIntermSwitch* node)
{
	static_cast<void>(visit);
	static_cast<void>(node);

	invokeError(node->getLoc(), "switch", "unsupported statement");
	return false;
}

void ParseTreeTraverser::visitSymbol(glslang::TIntermSymbol* node)
{
	validateType(node);
	const glslang::TQualifier& qualifier = node->getQualifier();

	if(qualifier.storage == glslang::EvqVaryingIn)
	{
		const uint32_t count = static_cast<uint32_t>(node->getVectorSize());
		_evaluatedOperand = InstructionOperand(qualifier.layoutLocation, count, 0);
	}
	else
	{
		processSymbol(node);
	}
}

bool ParseTreeTraverser::visitUnary(glslang::TVisit visit, glslang::TIntermUnary* node)
{
	static_cast<void>(visit);
	static_cast<void>(node);

	invokeError(node->getLoc(), "unary", "unsupported statement");
	return false;
}


// Internal

static uint32_t calculateTypeSize(const glslang::TType& type)
{
	if(type.getBasicType() == glslang::EbtVoid)
		return 0;

	return static_cast<uint32_t>(type.getVectorSize()) * unitSize;
}

static void invokeError(const glslang::TSourceLoc& location, const char* context, const char* message)
{
	glslang::TInfoSinkBase infoSink;
	infoSink.prefix(glslang::EPrefixError);
	infoSink.location(location);
	infoSink << '\'' << context << "' : " << message << '\n';

	throw std::runtime_error(infoSink.c_str());
}

static uint32_t relativeAddress(const uint32_t address)
{
	return relativeAddressMask | address;
}

static void validateType(const glslang::TIntermTyped* typedNode, const bool allowGlobal)
{
	const glslang::TType& type = typedNode->getType();

	switch(type.getBasicType())
	{
		case glslang::EbtFloat:
			break;

		case glslang::EbtVoid:
			return;

		default:
			invokeError(typedNode->getLoc(), type.getBasicTypeString().c_str(), "unsupported type");
			break;
	}

	const char* context = nullptr;

	if(type.isArray())
		context = "array";
	else if(type.isMatrix())
		context = "matrix";
	else if(type.isParameterized())
		context = "parameterised";

	if(context != nullptr)
		invokeError(typedNode->getLoc(), context, "unsupported type");

	switch(type.getQualifier().storage)
	{
		case glslang::EvqConst:
		case glslang::EvqIn:
		case glslang::EvqTemporary:
		case glslang::EvqVaryingIn:
			break;

		case glslang::EvqGlobal:
			if(allowGlobal)
				break;

		default:
			invokeError(typedNode->getLoc(), type.getStorageQualifierString(), "unsupported qualifier");
			break;
	}
}
