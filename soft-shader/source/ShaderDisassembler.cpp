/**
 * @file soft-shader/ShaderDisassembler.cpp
 *
 * SoftShader
 * Copyright 2020 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

#include <soft-shader/ShaderDisassembler.h>

#include <cassert>

using namespace SoftShader;

// Internal

static constexpr uint32_t absoluteAddressMask = 0x7FFFFFFF;

// Public

bool ShaderDisassembler::disassemble(const ByteVector& executable)
{
	_executableReader.reset(executable);

	try
	{
		validateSignature(executable);
		readHeader();
		readConstants();
		readCode();
	}
	catch(const std::runtime_error& exception)
	{
		_output = fmt::format("Disassemble failed:\n{}", exception.what());
		return false;
	}

	return true;
}

// Private

void ShaderDisassembler::validateSignature(const ByteVector& executable)
{
	if(executable.size() < codeSignature.size())
		invokeEofError("signature");

	if(!std::equal(codeSignature.begin(), codeSignature.end(), executable.begin()))
		throw std::runtime_error("ERROR: signature mismatch\n");

	_executableReader.seek(static_cast<uint32_t>(codeSignature.size()));
}

void ShaderDisassembler::readHeader()
{
	if(!_executableReader.hasBytesAvailable(sizeof(uint32_t)))
		invokeEofError("header");

	const uint32_t headerVersion = _executableReader.readUint32();
	const uint32_t headerMajorNumber = headerVersion >> 22;
	const uint32_t headerMinorNumber = (headerVersion >> 12) & 0x03FF;
	const uint32_t headerBuildNumber = headerVersion & 0x0FFF;
	_disassembly.append_formatted("SoftShader Executable - version {}.{}.{}\n\n", headerMajorNumber,
		headerMinorNumber, headerBuildNumber);
}

void ShaderDisassembler::readConstants()
{
	if(!_executableReader.hasBytesAvailable(sizeof(uint32_t)))
		invokeEofError("constants");

	const uint32_t constantsSize = _executableReader.readUint32();

	if(!_executableReader.hasBytesAvailable(constantsSize))
		invokeEofError("constants");

	_disassembly.append_formatted("Constants: {} bytes\n\n", constantsSize);
	_executableReader.seek(constantsSize);
}

void ShaderDisassembler::readCode()
{
	if(!_executableReader.hasBytesAvailable(2 * sizeof(uint32_t)))
		invokeEofError("code");

	_functionAddresses.emplace(_executableReader.readUint32(), "main");
	const uint32_t codeSize = _executableReader.readUint32();

	if(codeSize == 0)
		throw std::runtime_error("ERROR: executable code size is 0\n");

	if(!_executableReader.hasBytesAvailable(codeSize))
		invokeEofError("code");

	_codeBegin = _executableReader.position();

	while(!_executableReader.atEndOfFile())
		checkOpcode();

	_executableReader.reset(_codeBegin);

	while(!_executableReader.atEndOfFile())
	{
		checkForFunctionAddress();
		decodeOpcode();
	}
}

void ShaderDisassembler::checkForFunctionAddress()
{
	const uint32_t address = static_cast<uint32_t>(_executableReader.position() - _codeBegin);
	FunctionAddressMap::const_iterator iterator = _functionAddresses.find(address);

	if(iterator != _functionAddresses.end())
		_disassembly.append_formatted("{}{}:\n", address == 0 ? "" : "\n", iterator->second);
}

const char* ShaderDisassembler::readRegister()
{
	const uint32_t registerValue = _executableReader.readUint8();

	switch(static_cast<Register>(registerValue))
	{
		case Register::BP:
			return "bp";

		case Register::RP:
			return "rp";

		case Register::FP0:
			return "fp0";

		case Register::FP1:
			return "fp1";
	}

	invokeError(registerValue, "invalid register");
}

// Check methods

void ShaderDisassembler::checkOpcode()
{
	const uint32_t opcode = _executableReader.readUint8();

	switch(static_cast<Opcode>(opcode))
	{
		case Opcode::ADD:
			checkAdd();
			break;

		case Opcode::BPA:
		case Opcode::BPS:
			checkSimple(4);
			break;

		case Opcode::CALL:
			checkCall();
			break;

		case Opcode::LD:
			checkLd();
			break;

		case Opcode::MOV:
			checkMov();
			break;

		case Opcode::MOVF:
			checkSimple(6);
			break;

		case Opcode::RET:
			break;

		case Opcode::ST:
			checkSt();
			break;

		default:
			invokeError(opcode, "invalid opcode");
			break;
	}
}

void ShaderDisassembler::checkAdd()
{
	const uint32_t size = 1 + ((checkVariant() >> 4) == 0 ? 1 : 4);

	if(!_executableReader.hasBytesAvailable(size))
		invokeEofError("instruction");

	_executableReader.seek(size);
}

void ShaderDisassembler::checkCall()
{
	if(!_executableReader.hasBytesAvailable(4))
		invokeEofError("instruction");

	std::string functionName = fmt::format("func{}", _functionAddresses.size() - 1);
	_functionAddresses.emplace(_executableReader.readUint32(), std::move(functionName));
}

void ShaderDisassembler::checkLd()
{
	const uint32_t size = 2 + (checkVariant() == 0 ? 1 : 4);

	if(!_executableReader.hasBytesAvailable(size))
		invokeEofError("instruction");

	_executableReader.seek(size);
}

void ShaderDisassembler::checkMov()
{
	const uint32_t size = (checkVariant() >> 4) == 2 ? 8 : 5;

	if(!_executableReader.hasBytesAvailable(size))
		invokeEofError("instruction");

	_executableReader.seek(size);
}

void ShaderDisassembler::checkSt()
{
	const uint32_t size = 1 + (checkVariant() == 0 ? 1 : 4);

	if(!_executableReader.hasBytesAvailable(size))
		invokeEofError("instruction");

	_executableReader.seek(size);
}

// Decode methods

void ShaderDisassembler::decodeOpcode()
{
	_disassembly.append("    ");
	const Opcode opcode = static_cast<Opcode>(_executableReader.readUint8());

	switch(opcode)
	{
		case Opcode::ADD:
			decodeAdd();
			break;

		case Opcode::BPA:
		case Opcode::BPS:
			decodeBpaBps(opcode);
			break;

		case Opcode::CALL:
			decodeCall();
			break;

		case Opcode::LD:
			decodeLd();
			break;

		case Opcode::MOV:
			decodeMov();
			break;

		case Opcode::MOVF:
			decodeMovf();
			break;

		case Opcode::RET:
			_disassembly.append("ret");
			break;

		case Opcode::ST:
			decodeSt();
			break;
	}

	_disassembly.push_back('\n');
}

void ShaderDisassembler::decodeAdd()
{
	const uint32_t byte = _executableReader.readUint8();
	const uint32_t variant = byte >> 4;

	_disassembly.append_formatted("add {}, {}, ", (byte & 0x0F) + 1, readRegister());

	if(variant == 0)
		_disassembly.append(readRegister());
	else
		decodeAddress();
}

void ShaderDisassembler::decodeBpaBps(const Opcode opcode)
{
	switch(opcode)
	{
		case Opcode::BPA:
			_disassembly.append("bpa");
			break;

		case Opcode::BPS:
			_disassembly.append("bps");
			break;

		default:
			break;
	}

	_disassembly.append_formatted(" {}", _executableReader.readUint32());
}

void ShaderDisassembler::decodeCall()
{
	FunctionAddressMap::const_iterator iterator = _functionAddresses.find(_executableReader.readUint32());
	assert(iterator != _functionAddresses.end());
	_disassembly.append_formatted("call {}", iterator->second);
}

void ShaderDisassembler::decodeLd()
{
	const uint32_t variant = _executableReader.readUint8();

	_disassembly.append_formatted("ld {}, ", _executableReader.readUint16());

	if(variant == 0)
		_disassembly.append(readRegister());
	else
		decodeAddress();
}

void ShaderDisassembler::decodeMov()
{
	const uint32_t byte = _executableReader.readUint8();
	const uint32_t variant = byte >> 4;

	_disassembly.append_formatted("mov ");

	if(variant == 2)
		_disassembly.append_formatted("{}, ", (byte & 0x0F) + 1);

	if(variant == 0)
		_disassembly.append(readRegister());
	else
		decodeAddress();

	_disassembly.append(", ");

	if(variant == 1)
		_disassembly.append(readRegister());
	else
		decodeAddress();
}

void ShaderDisassembler::decodeMovf()
{
	const uint32_t byte = _executableReader.readUint8();
	const uint32_t variant = byte >> 4;

	_disassembly.append_formatted("movf {}, ", (byte & 0x0F) + 1);

	if(variant == 0)
	{
		_disassembly.append_formatted("{}, ", readRegister());
		decodeAddress();
	}
	else
	{
		decodeAddress();
		_disassembly.append_formatted(", {}", readRegister());
	}
}

void ShaderDisassembler::decodeSt()
{
	const uint32_t variant = _executableReader.readUint8();

	_disassembly.append_formatted("st {}, ", _executableReader.readUint8());

	if(variant == 0)
		_disassembly.append(readRegister());
	else
		decodeAddress();
}

void ShaderDisassembler::decodeAddress()
{
	const uint32_t address = _executableReader.readUint32();

	if((address & relativeAddressMask) == relativeAddressMask)
		_disassembly.append_formatted("({})", address & absoluteAddressMask);
	else
		_disassembly.append_formatted("[{}]", address);
}
