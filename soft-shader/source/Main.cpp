/**
 * @file soft-shader/Main.cpp
 *
 * SoftShader
 * Copyright 2020 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

#include <fstream>
#include <iostream>
#include <iterator>
#include <string>

#include <soft-shader/ShaderCodeGenerator.h>
#include <soft-shader/ShaderSourceParser.h>
#include <soft-shader/ShaderDisassembler.h>

using namespace SoftShader;

namespace glslang
{
	class TIntermediate;
}

int main(int argumentCount, char** arguments)
{
	static_cast<void>(argumentCount);
	static_cast<void>(arguments);

	std::ifstream file("vertex.glsl", std::ios::binary | std::ios::in);
	const std::string shaderSource(std::istreambuf_iterator<char>(file), {});
	file.close();

	ShaderSourceParser sourceParser;
	glslang::TIntermediate* parseTree = sourceParser.parse(ShaderType::Vertex, shaderSource);
	std::cout << sourceParser.output();

	if(parseTree == nullptr)
		return 1;

	ShaderCodeGenerator codeGenerator;
	bool result = codeGenerator.generate(parseTree);

	if(!result)
	{
		std::cout << codeGenerator.output();
		return 1;
	}

	ShaderDisassembler shaderDisassembler;
	result = shaderDisassembler.disassemble(codeGenerator.executable());

	if(!result)
	{
		std::cout << shaderDisassembler.output();
		return 1;
	}

	std::cout << shaderDisassembler.disassembly();
	return 0;
}
