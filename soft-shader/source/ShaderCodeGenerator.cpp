/**
 * @file soft-shader/ShaderCodeGenerator.cpp
 *
 * SoftShader
 * Copyright 2020 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

#include <soft-shader/ShaderCodeGenerator.h>

#include <cassert>
#include <cstddef>
#include <stdexcept>

#include <fmt/core.h>
#include <glslang/MachineIndependent/localintermediate.h>
#include <soft-shader/InstructionOperand.h>

using namespace SoftShader;

// Internal

static_assert(CHAR_BIT == 8, "The size of a byte is invalid.");
static_assert(alignof(float) == 4, "The alignment of 'float' is invalid.");
static_assert(sizeof(float) == 4, "The size of 'float' is invalid");

static uint32_t getInstructionSize(const Instruction& instruction);


// ShaderCodeGenerator

// Public

bool ShaderCodeGenerator::generate(glslang::TIntermediate* parseTree)
{
	_code.clear();
	_executable.clear();
	_functionAddresses.clear();
	_output.clear();

	ParseTreeTraverser parseTreeTraverser;

	try
	{
		parseTree->getTreeRoot()->traverse(&parseTreeTraverser);
	}
	catch(const std::runtime_error& exception)
	{
		_output = fmt::format("Code generation failed:\n{}", exception.what());
		return false;
	}

	const ParseResult result = parseTreeTraverser.result();
	resolveFunctionAddresses(result.instructions);
	_executable.append(codeSignature.begin(), codeSignature.end());
	generateHeader();
	generateConstants(result.constants);
	generateCode(result.instructions);

	return true;
}

// Private

void ShaderCodeGenerator::resolveFunctionAddresses(const InstructionVector& instructions)
{
	uint32_t address = 0;

	for(const Instruction& instruction : instructions)
	{
		if(!instruction.label.empty())
			_functionAddresses.emplace(instruction.label, address);

		address += getInstructionSize(instruction);
	}
}

void ShaderCodeGenerator::generateHeader()
{
	_executable << version;
}

void ShaderCodeGenerator::generateConstants(const ByteVector& constants)
{
	_executable << static_cast<uint32_t>(constants.size());
	_executable.append(constants.begin(), constants.end());
}

void ShaderCodeGenerator::generateCode(const InstructionVector& instructions)
{
	generateInstructions(instructions);
	FunctionAddressMap::const_iterator functionAddressIterator = _functionAddresses.find("main(");
	assert(functionAddressIterator != _functionAddresses.end());
	const ByteVector& code = _code.data();
	_executable << functionAddressIterator->second << static_cast<uint32_t>(code.size());
	_executable.append(code.begin(), code.end());
}

void ShaderCodeGenerator::generateInstructions(const InstructionVector& instructions)
{
	for(const Instruction& instruction : instructions)
	{
		_code << instruction.opcode;

		switch(instruction.opcode)
		{
			case Opcode::ADD:
			case Opcode::MOV:
			case Opcode::MOVF:
				generateAddMovMovf(instruction);
				break;

			case Opcode::BPA:
			case Opcode::BPS:
				_code << instruction.operand0;
				break;

			case Opcode::CALL:
				generateCall(instruction);
				break;

			case Opcode::LD:
				generateLd(instruction);
				break;

			case Opcode::RET:
				break;

			case Opcode::ST:
				_code << std::byte(instruction.variant) << instruction.operand0 << instruction.operand1;
				break;
		}
	}
}

// Generation methods

void ShaderCodeGenerator::generateAddMovMovf(const Instruction& instruction)
{
	const uint32_t count = instruction.operand1.count() - 1;
	const uint32_t variant = (instruction.variant << 4) | (count & 0x0F);
	_code << std::byte(variant) << instruction.operand0 << instruction.operand1;
}

void ShaderCodeGenerator::generateCall(const Instruction& instruction)
{
	FunctionAddressMap::const_iterator iterator = _functionAddresses.find(instruction.targetLabel);
	assert(iterator != _functionAddresses.end());
	_code << InstructionOperand(iterator->second);
}

void ShaderCodeGenerator::generateLd(const Instruction& instruction)
{
	_code << std::byte(instruction.variant) << ByteStream::LocationSize::E16 << instruction.operand0 <<
		ByteStream::LocationSize::E8 << instruction.operand1;
}


// Internal

static uint32_t getInstructionSize(const Instruction& instruction)
{
	switch(instruction.opcode)
	{
		case Opcode::ADD:
		case Opcode::ST:
			return 3 + (instruction.variant == 0 ? 1 : 4);

		case Opcode::BPA:
		case Opcode::BPS:
		case Opcode::CALL:
			return 5;

		case Opcode::LD:
			return 4 + (instruction.variant == 0 ? 1 : 4);

		case Opcode::MOV:
			return 2 + (instruction.variant == 2 ? 8 : 5);

		case Opcode::MOVF:
			return 7;

		case Opcode::RET:
			return 1;
	}

	return 0;
}
