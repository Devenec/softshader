/**
 * @file soft-shader/ShaderSourceParser.cpp
 *
 * SoftShader
 * Copyright 2020 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

#include <soft-shader/ShaderSourceParser.h>

#include <cstddef>
#include <string_view>

using namespace SoftShader;

// Internal

static constexpr TBuiltInResource builtInResources
{
	 32,    // maxLights
	 6,     // maxClipPlanes
	 32,    // maxTextureUnits
	 32,    // maxTextureCoords
	 64,    // maxVertexAttribs
	 4096,  // maxVertexUniformComponents
	 64,    // maxVaryingFloats
	 32,    // maxVertexTextureImageUnits
	 80,    // maxCombinedTextureImageUnits
	 32,    // maxTextureImageUnits
	 4096,  // maxFragmentUniformComponents
	 32,    // maxDrawBuffers
	 128,   // maxVertexUniformVectors
	 8,     // maxVaryingVectors
	 16,    // maxFragmentUniformVectors
	 16,    // maxVertexOutputVectors
	 15,    // maxFragmentInputVectors
	-8,     // minProgramTexelOffset
	 7,     // maxProgramTexelOffset
	 8,     // maxClipDistances
	 65535, // maxComputeWorkGroupCountX
	 65535, // maxComputeWorkGroupCountY
	 65535, // maxComputeWorkGroupCountZ
	 1024,  // maxComputeWorkGroupSizeX
	 1024,  // maxComputeWorkGroupSizeY
	 64,    // maxComputeWorkGroupSizeZ
	 1024,  // maxComputeUniformComponents
	 16,    // maxComputeTextureImageUnits
	 8,     // maxComputeImageUniforms
	 8,     // maxComputeAtomicCounters
	 1,     // maxComputeAtomicCounterBuffers
	 60,    // maxVaryingComponents
	 64,    // maxVertexOutputComponents
	 64,    // maxGeometryInputComponents
	 128,   // maxGeometryOutputComponents
	 128,   // maxFragmentInputComponents
	 8,     // maxImageUnits
	 8,     // maxCombinedImageUnitsAndFragmentOutputs
	 8,     // maxCombinedShaderOutputResources
	 0,     // maxImageSamples
	 0,     // maxVertexImageUniforms
	 0,     // maxTessControlImageUniforms
	 0,     // maxTessEvaluationImageUniforms
	 0,     // maxGeometryImageUniforms
	 8,     // maxFragmentImageUniforms
	 8,     // maxCombinedImageUniforms
	 16,    // maxGeometryTextureImageUnits
	 256,   // maxGeometryOutputVertices
	 1024,  // maxGeometryTotalOutputComponents
	 1024,  // maxGeometryUniformComponents
	 64,    // maxGeometryVaryingComponents
	 128,   // maxTessControlInputComponents
	 128,   // maxTessControlOutputComponents
	 16,    // maxTessControlTextureImageUnits
	 1024,  // maxTessControlUniformComponents
	 4096,  // maxTessControlTotalOutputComponents
	 128,   // maxTessEvaluationInputComponents
	 128,   // maxTessEvaluationOutputComponents
	 16,    // maxTessEvaluationTextureImageUnits
	 1024,  // maxTessEvaluationUniformComponents
	 120,   // maxTessPatchComponents
	 32,    // maxPatchVertices
	 64,    // maxTessGenLevel
	 16,    // maxViewports
	 0,     // maxVertexAtomicCounters
	 0,     // maxTessControlAtomicCounters
	 0,     // maxTessEvaluationAtomicCounters
	 0,     // maxGeometryAtomicCounters
	 8,     // maxFragmentAtomicCounters
	 8,     // maxCombinedAtomicCounters
	 1,     // maxAtomicCounterBindings
	 0,     // maxVertexAtomicCounterBuffers
	 0,     // maxTessControlAtomicCounterBuffers
	 0,     // maxTessEvaluationAtomicCounterBuffers
	 0,     // maxGeometryAtomicCounterBuffers
	 1,     // maxFragmentAtomicCounterBuffers
	 1,     // maxCombinedAtomicCounterBuffers
	 16384, // maxAtomicCounterBufferSize
	 4,     // maxTransformFeedbackBuffers
	 64,    // maxTransformFeedbackInterleavedComponents
	 8,     // maxCullDistances
	 8,     // maxCombinedClipAndCullDistances
	 4,     // maxSamples
	 256,   // maxMeshOutputVerticesNV
	 512,   // maxMeshOutputPrimitivesNV
	 32,    // maxMeshWorkGroupSizeX_NV
	 1,     // maxMeshWorkGroupSizeY_NV
	 1,     // maxMeshWorkGroupSizeZ_NV
	 32,    // maxTaskWorkGroupSizeX_NV
	 1,     // maxTaskWorkGroupSizeY_NV
	 1,     // maxTaskWorkGroupSizeZ_NV
	 4,     // maxMeshViewCountNV

	// limits
	{
		true, // nonInductiveForLoops
		true, // whileLoops
		true, // doWhileLoops
		true, // generalUniformIndexing
		true, // generalAttributeMatrixVectorIndexing
		true, // generalVaryingIndexing
		true, // generalSamplerIndexing
		true, // generalVariableIndexing
		true  // generalConstantMatrixVectorIndexing
	}
};

static EShLanguage getShaderLanguage(const ShaderType shaderType);
static void stripInfoLog(std::string_view& infoLog);


// Public

glslang::TIntermediate* ShaderSourceParser::parse(const ShaderType shaderType, const std::string& shaderSource)
{
	_output.clear();
	const EShLanguage shaderLanguage = getShaderLanguage(shaderType);
	bool result = parseShader(shaderLanguage, shaderSource);

	if(result)
		result = linkProgram();

	return result ? _program->getIntermediate(shaderLanguage) : nullptr;
}

// Private

bool ShaderSourceParser::parseShader(const EShLanguage shaderLanguage, const std::string& shaderSource)
{
	_shader = std::make_unique<glslang::TShader>(shaderLanguage);
	const char* shaderSourceCharacters = shaderSource.c_str();
	_shader->setStrings(&shaderSourceCharacters, 1);
	const bool result = _shader->parse(&builtInResources, 450, ECoreProfile, true, false, EShMsgDefault);
	formatOutput(result, " ", _shader->getInfoLog());

	return result;
}

bool ShaderSourceParser::linkProgram()
{
	_program = std::make_unique<glslang::TProgram>();
	_program->addShader(_shader.get());
	const bool result = _program->link(EShMsgDefault);
	formatOutput(result, " (linking stage) ", _shader->getInfoLog());

	return result;
}

void ShaderSourceParser::formatOutput(const bool result, const std::string_view& context,
	std::string_view infoLog)
{
	if(result && infoLog.empty())
		return;

	stripInfoLog(infoLog);

	_output = "Parsing the source";
	_output.append(context);
	_output.append(result ? "succeeded with message(s):\n" : "failed:\n");
	_output.append(infoLog);
	_output.push_back('\n');
}


// Internal

static EShLanguage getShaderLanguage(const ShaderType shaderType)
{
	switch(shaderType)
	{
		case ShaderType::Vertex:
			return EShLangVertex;
	}

	return EShLangVertex;
}

static void stripInfoLog(std::string_view& infoLog)
{
	std::size_t whitespacePosition = infoLog.find_last_not_of("\t\n\r ");

	if(whitespacePosition != std::string::npos)
		infoLog.remove_suffix(infoLog.length() - whitespacePosition);
}
