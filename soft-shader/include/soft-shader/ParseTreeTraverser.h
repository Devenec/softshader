/**
 * @file soft-shader/ParseTreeTraverser.h
 *
 * SoftShader
 * Copyright 2020 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

#pragma once

#include <cassert>
#include <stdint.h>
#include <string>
#include <unordered_map>
#include <vector>

#include <glslang/Include/intermediate.h>
#include <soft-shader/InstructionOperand.h>
#include <soft-shader/ShaderCode.h>

namespace SoftShader
{
	struct Instruction
	{
		std::string label;
		std::string targetLabel;
		InstructionOperand operand0;
		InstructionOperand operand1;
		uint32_t variant;
		Opcode opcode;

		Instruction() :
			variant(0),
			opcode(Opcode::ADD) { }

		Instruction(const Opcode opcode, const uint32_t variant, const InstructionOperand& operand0,
			const InstructionOperand& operand1, const std::string& label, const std::string& targetLabel) :
			label(label),
			targetLabel(targetLabel),
			operand0(operand0),
			operand1(operand1),
			variant(variant),
			opcode(opcode) { }
	};

	using InstructionVector = std::vector<Instruction>;

	struct ParseResult
	{
		const ByteVector constants;
		const InstructionVector& instructions;

		ParseResult(const ByteVector& constants, const InstructionVector& instructions) :
			constants(constants),
			instructions(instructions) { }
	};

	class ParseTreeTraverser : public glslang::TIntermTraverser
	{
	public:

		ParseTreeTraverser() :
			_evaluatedOperand(invalidAddress),
			_memoryPointer(0) { }

		ParseTreeTraverser(const ParseTreeTraverser& parseTreeTraverser) = delete;
		ParseTreeTraverser(ParseTreeTraverser&& parseTreeTraverser) = delete;

		~ParseTreeTraverser() = default;

		ParseResult result()
		{
			return ParseResult(_constants, _instructions);
		}

		ParseTreeTraverser& operator =(const ParseTreeTraverser& parseTreeTraverser) = delete;
		ParseTreeTraverser& operator =(ParseTreeTraverser&& parseTreeTraverser) = delete;

	private:

		struct Symbol
		{
			uint32_t address;

			explicit Symbol(const uint32_t address) :
				address(address) { }
		};

		using SymbolID  = int64_t;
		using SymbolMap = std::unordered_map<SymbolID, Symbol>;
		
		static constexpr uint32_t invalidAddress = 0x40000000;

		std::string _activeLabel;
		ByteVector _constants;
		InstructionVector _instructions;
		SymbolMap _symbols;
		InstructionOperand _evaluatedOperand;
		uint32_t _memoryPointer;

		void addInstruction(const Opcode opcode, const uint32_t variant = 0,
			const InstructionOperand& operand0 = InstructionOperand(),
			const InstructionOperand& operand1 = InstructionOperand(),
			const glslang::TString& targetLabel = glslang::TString())
		{
			_instructions.emplace_back(opcode, variant, operand0, operand1, _activeLabel,
				std::string(targetLabel));

			_activeLabel.clear();
		}

		void addInstructionAdd(const InstructionOperand& operand0, const InstructionOperand& operand1)
		{
			assert
			(
				(operand0.isRegister() && operand1.isRegister()) ||
				(operand0.isRegister() && operand1.isAddress())
			);

			const uint32_t variant = operand1.isRegister() ? 0 : 1;
			addInstruction(Opcode::ADD, variant, operand0, operand1);
		}

		void addInstructionLd(const InstructionOperand& operand0, const InstructionOperand& operand1)
		{
			assert
			(
				(operand0.isLocation() && operand1.isRegister()) ||
				(operand0.isLocation() && operand1.isAddress())
			);

			const uint32_t variant = operand1.isRegister() ? 0 : 1;
			addInstruction(Opcode::LD, variant, operand0, operand1);
		}

		void addInstructionMov(const InstructionOperand& operand0, const InstructionOperand& operand1);

		void addInstructionMovf(const InstructionOperand& operand0, const InstructionOperand& operand1)
		{
			assert
			(
				(operand0.isRegister() && operand1.isAddress()) ||
				(operand0.isAddress() && operand1.isRegister())
			);

			const uint32_t variant = operand0.isRegister() ? 0 : 1;
			addInstruction(Opcode::MOVF, variant, operand0, operand1);
		}

		void addInstructionSt(const InstructionOperand& operand0, const InstructionOperand& operand1)
		{
			assert
			(
				(operand0.isLocation() && operand1.isRegister()) ||
				(operand0.isLocation() && operand1.isAddress())
			);

			const uint32_t variant = operand1.isRegister() ? 0 : 1;
			addInstruction(Opcode::ST, variant, operand0, operand1);
		}

		void processBranchReturn(glslang::TIntermBranch* node);
		void processConstruct(glslang::TIntermAggregate* node);
		void processFunction(glslang::TIntermAggregate* node);
		void processFunctionCall(glslang::TIntermAggregate* node);
		void processLoadOrMove(const InstructionOperand& destinationOperand,
			const InstructionOperand& sourceOperand);

		void processMove(const InstructionOperand& destinationOperand,
			const InstructionOperand& sourceOperand);

		void processOperationAdd(glslang::TIntermBinary* node);
		void processOperationAssign(glslang::TIntermBinary* node);
		void processOperationIndexStructure(const glslang::TIntermBinary* node);
		void processSymbol(const glslang::TIntermSymbol* node);

		bool visitAggregate(glslang::TVisit visit, glslang::TIntermAggregate* node) override;
		bool visitBinary(glslang::TVisit visit, glslang::TIntermBinary* node) override;
		bool visitBranch(glslang::TVisit visit, glslang::TIntermBranch* node) override;
		void visitConstantUnion(glslang::TIntermConstantUnion* node) override;
		bool visitLoop(glslang::TVisit visit, glslang::TIntermLoop* node) override;
		bool visitSelection(glslang::TVisit visit, glslang::TIntermSelection* node) override;
		bool visitSwitch(glslang::TVisit visit, glslang::TIntermSwitch* node) override;
		void visitSymbol(glslang::TIntermSymbol* node) override;
		bool visitUnary(glslang::TVisit visit, glslang::TIntermUnary* node) override;
	};
}
