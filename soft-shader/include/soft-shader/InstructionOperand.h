/**
 * @file soft-shader/InstructionOperand.h
 *
 * SoftShader
 * Copyright 2020 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

#pragma once

#include <stdint.h>
#include <soft-shader/ShaderCode.h>

namespace SoftShader
{
	class InstructionOperand
	{
	public:

		enum class Type
		{
			Address,
			Location,
			Register
		};

		InstructionOperand() :
			_count(0),
			_numericValue(0),
			_type(Type::Address) { }

		InstructionOperand(const uint32_t address, const uint32_t count = 1) :
			_count(count),
			_numericValue(address),
			_type(Type::Address) { }

		InstructionOperand(const uint32_t location, const uint32_t count, const uint32_t dummy) :
			_count(count),
			_numericValue(location),
			_type(Type::Location)
		{
			static_cast<void>(dummy);
		}

		InstructionOperand(const Register registerValue, const uint32_t count = 1) :
			_count(count),
			_register(registerValue),
			_type(Type::Register) { }

		InstructionOperand(const InstructionOperand& instructionOperand) = default;
		InstructionOperand(InstructionOperand&& instructionOperand) = default;

		~InstructionOperand() = default;

		uint32_t count() const
		{
			return _count;
		}

		bool isAddress() const
		{
			return _type == Type::Address;
		}

		bool isLocation() const
		{
			return _type == Type::Location;
		}

		bool isRegister() const
		{
			return _type == Type::Register;
		}

		uint32_t numericValue() const
		{
			return _numericValue;
		}

		Register registerValue() const
		{
			return _register;
		}

		Type type() const
		{
			return _type;
		}

		InstructionOperand& operator =(const InstructionOperand& instructionOperand) = default;
		InstructionOperand& operator =(InstructionOperand && instructionOperand) = default;

	private:

		union
		{
			uint32_t _numericValue;
			Register _register;
		};

		uint32_t _count;
		Type _type;
	};

	inline bool operator ==(const InstructionOperand& operand0, const InstructionOperand& operand1)
	{
		if(operand0.type() != operand1.type())
			return false;

		return operand0.isRegister() ? (operand0.registerValue() == operand1.registerValue()) :
			(operand0.numericValue() == operand1.numericValue());
	}

	inline bool operator !=(const InstructionOperand& operand0, const InstructionOperand& operand1)
	{
		return !(operand0 == operand1);
	}
}
