/**
 * @file soft-shader/ShaderDisassembler.h
 *
 * SoftShader
 * Copyright 2020 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

#pragma once

#include <algorithm>
#include <cstddef>
#include <iterator>
#include <stdexcept>
#include <stdint.h>
#include <string>
#include <unordered_map>
#include <utility>

#include <fmt/core.h>
#include <soft-shader/ShaderCode.h>

namespace SoftShader
{
	class ShaderDisassembler
	{
	public:

		ShaderDisassembler() = default;

		ShaderDisassembler(const ShaderDisassembler& shaderDisassembler) = delete;
		ShaderDisassembler(ShaderDisassembler&& shaderDisassembler) = delete;

		~ShaderDisassembler() = default;

		bool disassemble(const ByteVector& executable);

		std::string disassembly() const
		{
			return _disassembly;
		}

		const std::string& output() const
		{
			return _output;
		}

		ShaderDisassembler& operator =(const ShaderDisassembler& shaderDisassembler) = delete;
		ShaderDisassembler& operator =(ShaderDisassembler&& shaderDisassembler) = delete;

	private:

		class FormattedString : public std::string
		{
		public:

			FormattedString() = default;

			FormattedString(const FormattedString& formattedString) = default;
			FormattedString(FormattedString&& formattedString) = default;

			~FormattedString() = default;

			template<typename... Arguments>
			FormattedString& append_formatted(const char* format, Arguments&&... arguments)
			{
				fmt::format_to(std::back_inserter<std::string>(*this), format,
					std::forward<Arguments>(arguments)...);

				return *this;
			}

			FormattedString& operator =(const FormattedString& formattedString) = default;
			FormattedString& operator =(FormattedString&& formattedString) = default;
		};

		class ExecutableReader
		{
		public:

			ExecutableReader() = default;

			ExecutableReader(const ExecutableReader& executableReader) = delete;
			ExecutableReader(ExecutableReader&& executableReader) = delete;

			~ExecutableReader() = default;

			bool atEndOfFile() const
			{
				return _iterator == _iteratorEnd;
			}

			bool hasBytesAvailable(const uint32_t byte_count) const
			{
				return static_cast<std::ptrdiff_t>(byte_count) <= _iteratorEnd - _iterator;
			}

			ByteVector::const_iterator position() const
			{
				return _iterator;
			}

			uint32_t readUint8()
			{
				return static_cast<uint8_t>(*(_iterator++));
			}

			uint32_t readUint16()
			{
				uint16_t value = 0;
				std::copy_n(_iterator, sizeof(uint16_t), reinterpret_cast<std::byte*>(&value));
				_iterator += sizeof(uint16_t);

				return value;
			}

			uint32_t readUint32()
			{
				uint32_t value = 0;
				std::copy_n(_iterator, sizeof(uint32_t), reinterpret_cast<std::byte*>(&value));
				_iterator += sizeof(uint32_t);

				return value;
			}

			void reset(const ByteVector& executable)
			{
				_iterator = executable.begin();
				_iteratorEnd = executable.end();
			}

			void reset(const ByteVector::const_iterator iterator)
			{
				_iterator = iterator;
			}

			void seek(const uint32_t offset)
			{
				_iterator += offset;
			}

			ExecutableReader& operator =(const ExecutableReader & executableReader) = delete;
			ExecutableReader& operator =(ExecutableReader && executableReader) = delete;

		private:

			ByteVector::const_iterator _iterator;
			ByteVector::const_iterator _iteratorEnd;
		};

		using FunctionAddressMap = std::unordered_map<uint32_t, std::string>;

		ByteVector::const_iterator _codeBegin;
		FormattedString _disassembly;
		ExecutableReader _executableReader;
		FunctionAddressMap _functionAddresses;
		std::string _output;

		void validateSignature(const ByteVector& executable);
		void readHeader();
		void readConstants();
		void readCode();

		void invokeEofError(const char* context) const
		{
			invokeError(context, "unexpected end-of-file encountered");
		}

		void checkForFunctionAddress();

		template<typename T>
		[[noreturn]]
		void invokeError(const T context, const char* message) const
		{
			const uint32_t address = static_cast<uint32_t>(_executableReader.position() - _codeBegin);
			const std::string exceptionMessage = fmt::format("ERROR: <0x{:0>8x}>: '{}' : {}\n", address,
				context, message);

			throw std::runtime_error(exceptionMessage);
		}

		const char* readRegister();

		void checkOpcode();
		void checkAdd();
		void checkCall();
		void checkLd();
		void checkMov();
		void checkSt();

		void checkSimple(const uint32_t size)
		{
			if(!_executableReader.hasBytesAvailable(size))
				invokeEofError("instruction");

			_executableReader.seek(size);
		}

		uint32_t checkVariant()
		{
			if(!_executableReader.hasBytesAvailable(1))
				invokeEofError("instruction");

			return _executableReader.readUint8();
		}

		void decodeOpcode();
		void decodeAdd();
		void decodeBpaBps(const Opcode opcode);
		void decodeCall();
		void decodeLd();
		void decodeMov();
		void decodeMovf();
		void decodeSt();
		void decodeAddress();
	};
}
