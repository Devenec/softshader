/**
 * @file soft-shader/ShaderSourceParser.h
 *
 * SoftShader
 * Copyright 2020 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

#pragma once

#include <memory>
#include <string>

#include <glslang/Public/ShaderLang.h>

namespace SoftShader
{
	enum class ShaderType
	{
		Vertex
	};

	class ShaderSourceParser
	{
	public:

		ShaderSourceParser()
		{
			glslang::InitializeProcess();
		}

		ShaderSourceParser(const ShaderSourceParser& ShaderSourceParser) = delete;
		ShaderSourceParser(ShaderSourceParser&& shaderSourceParser) = delete;

		~ShaderSourceParser()
		{
			glslang::FinalizeProcess();
		}

		const std::string& output() const
		{
			return _output;
		}

		glslang::TIntermediate* parse(const ShaderType shaderType, const std::string& shaderSource);

		ShaderSourceParser& operator =(const ShaderSourceParser& shaderSourceParser) = delete;
		ShaderSourceParser& operator =(ShaderSourceParser&& shaderSourceParser) = delete;

	private:

		std::unique_ptr<glslang::TProgram> _program;
		std::unique_ptr<glslang::TShader> _shader;
		std::string _output;

		bool parseShader(const EShLanguage shaderLanguage, const std::string& shaderSource);
		bool linkProgram();
		void formatOutput(const bool result, const std::string_view& context, std::string_view infoLog);
	};
}
