/**
 * @file soft-shader/ByteStream.h
 *
 * SoftShader
 * Copyright 2020 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

#pragma once

#include <cstddef>
#include <stdint.h>

#include <soft-shader/InstructionOperand.h>
#include <soft-shader/ShaderCode.h>

namespace SoftShader
{
	class ByteStream
	{
	public:

		enum class LocationSize
		{
			E8,
			E16
		};

		ByteStream() :
			_locationSize(LocationSize::E8) { }

		ByteStream(const ByteStream& byteStream) = delete;
		ByteStream(ByteStream&& byteStream) = delete;

		~ByteStream() = default;

		template<typename InputIterator>
		ByteStream& append(const InputIterator begin, const InputIterator end)
		{
			_data.insert(_data.end(), begin, end);
			return *this;
		}

		void clear()
		{
			_data.clear();
		}

		const ByteVector& data() const
		{
			return _data;
		}

		ByteStream& operator =(const ByteStream& byteStream) = delete;
		ByteStream& operator =(ByteStream&& byteStream) = delete;

		ByteStream& operator <<(const std::byte value)
		{
			_data.push_back(value);
			return *this;
		}

		ByteStream& operator <<(const uint16_t value)
		{
			const std::byte* valuePointer = reinterpret_cast<const std::byte*>(&value);
			return append(valuePointer, valuePointer + sizeof(uint16_t));
		}

		ByteStream& operator <<(const uint32_t value)
		{
			const std::byte* valuePointer = reinterpret_cast<const std::byte*>(&value);
			return append(valuePointer, valuePointer + sizeof(uint32_t));
		}

		ByteStream& operator <<(const InstructionOperand& operand)
		{
			switch(operand.type())
			{
				case InstructionOperand::Type::Address:
					return *this << operand.numericValue();

				case InstructionOperand::Type::Location:
					return _locationSize == LocationSize::E8 ? (*this << std::byte(operand.numericValue())) :
						(*this << static_cast<uint16_t>(operand.numericValue()));

				case InstructionOperand::Type::Register:
					return *this << operand.registerValue();
			}

			return *this;
		}

		ByteStream& operator <<(const LocationSize& locationSize)
		{
			_locationSize = locationSize;
			return *this;
		}

		ByteStream& operator <<(const Opcode opcode)
		{
			return *this << std::byte(opcode);
		}

		ByteStream& operator <<(const Register registerValue)
		{
			return *this << std::byte(registerValue);
		}

	private:

		ByteVector _data;
		LocationSize _locationSize;
	};
}
