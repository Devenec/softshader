/**
 * @file soft-shader/ShaderCode.h
 *
 * SoftShader
 * Copyright 2020 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

#pragma once

#include <array>
#include <cstddef>
#include <stdint.h>
#include <vector>

namespace SoftShader
{
	enum class ExternalMemory : uint8_t
	{
		Input   = 0,
		Output  = 1,
		Uniform = 2
	};

	enum class Opcode : uint8_t
	{
		ADD  = 0,
		BPA  = 1,
		BPS  = 2,
		CALL = 3,
		LD   = 4,
		MOV  = 5,
		MOVF = 6,
		RET  = 7,
		ST   = 8
	};

	enum class Register : uint8_t
	{
		BP  = 0,
		IP  = 1,
		RP  = 2,
		FP0 = 3,
		FP1 = 4
	};

	using ByteVector = std::vector<std::byte>;

	static constexpr uint32_t majorNumber = 0;
	static constexpr uint32_t minorNumber = 0;
	static constexpr uint32_t buildNumber = 1;
	static constexpr uint32_t version     = (majorNumber << 22) | (minorNumber << 12) | buildNumber;

	static constexpr std::array<std::byte, 4> codeSignature
	{
		std::byte('S'),
		std::byte('f'),
		std::byte('S'),
		std::byte('h')
	};

	static constexpr uint32_t relativeAddressMask = 0x80000000;
}
