/**
 * @file soft-shader/ShaderCodeGenerator.h
 *
 * SoftShader
 * Copyright 2020 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

#pragma once

#include <stdint.h>
#include <string>
#include <unordered_map>

#include <soft-shader/ByteStream.h>
#include <soft-shader/ParseTreeTraverser.h>
#include <soft-shader/ShaderCode.h>

namespace glslang
{
	class TIntermediate;
}

namespace SoftShader
{
	class ShaderCodeGenerator
	{
	public:

		ShaderCodeGenerator() = default;

		ShaderCodeGenerator(const ShaderCodeGenerator& shaderCodeGenerator) = delete;
		ShaderCodeGenerator(ShaderCodeGenerator&& shaderCodeGenerator) = delete;

		~ShaderCodeGenerator() = default;

		const ByteVector& executable() const
		{
			return _executable.data();
		}

		bool generate(glslang::TIntermediate* parseTree);

		const std::string& output() const
		{
			return _output;
		}

		ShaderCodeGenerator& operator =(const ShaderCodeGenerator& shaderCodeGenerator) = delete;
		ShaderCodeGenerator& operator =(ShaderCodeGenerator&& shaderCodeGenerator) = delete;

	private:

		using FunctionAddressMap = std::unordered_map<std::string, uint32_t>;

		ByteStream _code;
		ByteStream _executable;
		FunctionAddressMap _functionAddresses;
		std::string _output;

		void resolveFunctionAddresses(const InstructionVector& instructions);
		void generateHeader();
		void generateConstants(const ByteVector& constants);
		void generateCode(const InstructionVector& instructions);
		void generateInstructions(const InstructionVector& instructions);

		void generateAddMovMovf(const Instruction& instruction);
		void generateCall(const Instruction& instruction);
		void generateLd(const Instruction& instruction);
	};
}
